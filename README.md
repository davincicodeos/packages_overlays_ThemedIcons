# Themed Icons for Pixel Launcher

## Credits

- Structure heavily inspired by the PE project: [here](https://github.com/PixelExperience/packages_overlays_ThemeIcons)
- Initial icons extracted from [Marco](https://github.com/Hack64)'s Picsel Inexperience project
