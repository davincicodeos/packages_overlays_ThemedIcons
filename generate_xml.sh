#!/bin/bash

# initialize a semaphore with a given number of tokens
open_sem(){
    mkfifo pipe-$$
    exec 3<>pipe-$$
    rm pipe-$$
    local i=$1
    for((;i>0;i--)); do
        printf %s 000 >&3
    done
}

# run the given command asynchronously and pop/push tokens
run_with_lock(){
    local x
    # this read waits until there is something to read
    read -u 3 -n 3 x && ((0==x)) || exit $x
    (
     ( "$@"; )
    # push the return code of the command to the semaphore
    printf '%.3d' $? >&3
    )&
}

open_sem $(nproc --all)

function traverse() {
    for file in "$1"/*
    do
        if [ ! -d "${file}" ]; then
            if [ "${file##*.}" = "png" ]; then
                run_with_lock trace $file
            fi
        else
            traverse "${file}"
        fi
    done
}

function trace() {
    echo "Creating XML for $1"
    vd="${1%.*}.xml"

    # Make all non-transparent pixels black to improve trace accuracy
    # Convert PNG to PNM
    # Trace PNM to SVG
    # Convert SVG to VD XML
    convert $1 xc:"#000000" -channel RGB -clut - | pngtopnm -mix | potrace --svg | svg2vd -i - -o - > $vd
}

traverse "$1"
